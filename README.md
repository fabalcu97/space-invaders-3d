# Space Invaders 3D


## How to install
  - Clone the repo and open it with unity
  - Wait until unity installs every dependency

## How to play
  - Execute the project either compiling or pressing the play button
  - One player must set the Host by pressing H or clickng on Host
  - The other players must connect by the LAN field, introducing the host IP.
  - Have fun!

## [Demo](https://www.youtube.com/watch?v=2tuDxGsoz0U)