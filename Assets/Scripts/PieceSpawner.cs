﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;

public class PieceSpawner : NetworkBehaviour
{

  public GameObject piecePrefab;

  public void SpawnPiece (Enemy obj) {
    float rnd = UnityEngine.Random.Range(0.0f, 1.0f);
    var piecePosition = obj.transform.position;
		var pieceRotation = Quaternion.Euler(0.0f, 20.0f, 0.0f);
		var piece = Instantiate(piecePrefab, piecePosition, pieceRotation);
    piece.gameObject.GetComponent<Piece>().SetType(rnd);
    NetworkServer.Spawn(piece);
    // Debug.Log("RND =>" + rnd);
  }

  public override void OnStartServer()
  {

  }

  public void Update()
  {

  }
}
