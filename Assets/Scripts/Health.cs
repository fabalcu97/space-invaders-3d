﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Health : NetworkBehaviour {

    public const int maxHealth = 200;
    public const int maxDefence = 200;

    public bool destroyOnDeath;

    [SyncVar(hook = "OnChangeHealth")]
    public int currentHealth = maxHealth;

    [SyncVar(hook = "OnChangeDefense")]
    public int currentDefence = maxDefence;

    public RectTransform healthBar;
    public RectTransform defenseBar;

    public void TakeDamage(int amount) {
        if (!isServer)
            return;
        
        var enemy = gameObject.GetComponent<Enemy>();
        // Debug.Log("Defensa: " + currentDefence);
        if(currentDefence >=0 && !enemy){
            currentDefence -= amount;
            // Debug.Log("def: " + currentDefence);
            return;
        }

        else{
            // Debug.Log("health: " + currentDefence);
            currentHealth -= amount;
            if (currentHealth <= 0) {
                if (destroyOnDeath) {
                    if (enemy != null) {
                        GameObject.Find("Enemy Spawner").GetComponent<PieceSpawner>().SpawnPiece(enemy);
                    }
                    Destroy(gameObject);
                } 
                else {
                    currentHealth = maxHealth;
                    // RpcRespawn();
                }
            }
        }
    }

    void OnChangeHealth (int currentHealth)
    {
        if (healthBar) {
            healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
        }
    }

    void OnChangeDefense (int currentdefense)
    {
        if (defenseBar) {
            defenseBar.sizeDelta = new Vector2(currentdefense, defenseBar.sizeDelta.y);
        }
    }

    [ClientRpc]
    void RpcRespawn(){
        if (isLocalPlayer){
            transform.position = Vector3.zero;
        }
    }
}