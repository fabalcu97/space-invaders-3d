﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using Random=UnityEngine.Random;

public class Enemy : NetworkBehaviour {

	public GameObject bulletPrefab;
	public Transform bulletSpawn;
	public GameObject piecePrefab;
	public float bulletLife;
	public int direction;
	private float currentTime;
	private GameObject bullet;
	private Vector3 lookAtVector;
	[Command]
  void CmdFire() {
    bullet = Instantiate(
      bulletPrefab,
      bulletSpawn.position,
      bulletSpawn.rotation
    );

    bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 10;
    NetworkServer.Spawn(bullet);

    Destroy(bullet, bulletLife);
  }


	void Start () {
		currentTime = 0;
		lookAtVector = new Vector3(UnityEngine.Random.Range(-3.0f, 3.0f), 0.0f, 0.0f);
	}

	public void SetMovement (int value) {
		if (value >= 0) {
			direction = 1;
		}
		if (value < 0) {
			direction = -1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if (currentTime > 2 && !bullet) {
			currentTime = 0;
			CmdFire();
		}
		if (transform.position.y >= 10 || transform.position.y <= -10) {
			direction *= -1;
		}
		transform.position = new Vector3(transform.position.x, transform.position.y+1.5f*direction*Time.deltaTime, transform.position.z);
		transform.LookAt(lookAtVector);
	}

	// [Command]
	public void SpawnPiece(){
		var piecePosition = transform.position;
		var pieceRotation = Quaternion.Euler(0.0f, 20.0f, 0.0f);
		var piece = Instantiate(piecePrefab, piecePosition, pieceRotation);
		NetworkServer.Spawn(piece);
	}

}
