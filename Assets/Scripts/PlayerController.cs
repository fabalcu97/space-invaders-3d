﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{

  public GameObject bulletPrefab;
  public Transform bulletSpawn;
  public float bulletLife;
	GameObject myCam = null;

  public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
 public RotationAxes axes = RotationAxes.MouseXAndY;
 public float sensitivityX = 2F;
 public float sensitivityY = 2F;
 public float minimumX = -360F;
 public float maximumX = 360F;
 public float minimumY = -60F;
 public float maximumY = 60F;
 float rotationY = 0F;
 public int type;
 public int medicine;
 public int amo;
 public int defense;

  public Text typeText;
 private Vector3 centerPosition;
 private float radius = 10f;


	GameObject myVisor = null;
  [Command]
  void CmdFire() {
    var bullet = (GameObject)Instantiate(
      bulletPrefab,
      bulletSpawn.position,
      bulletSpawn.rotation
    );

    bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 40;
    NetworkServer.Spawn(bullet);

    Destroy(bullet, bulletLife);
  }

  public void SetType(int type) {
    this.type = type;
    // SetColorByType();
  }

  // Use this for initialization
  void Start() {
		myCam = GameObject.Find("Main Camera").gameObject;
		myVisor = this.gameObject.transform.Find("Visor").gameObject;
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
    bulletLife = 5f;
    centerPosition = transform.localPosition;
  }

  void OnGUI(){
     GUI.Box(new Rect(Screen.width/2,Screen.height/2, 10, 10), "");
  }

  void Update() {
    // Debug.Log("Type => " + type);
    // SetColorByType();
    if (!isLocalPlayer) {
      return;
    }
    SetTextByType();

    if (Input.GetKeyDown(KeyCode.Escape)) {
      if (Cursor.visible) {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.None;
      } else {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked;
      }
    }

		myCam.transform.position= myVisor.transform.position;
		myCam.transform.rotation= myVisor.transform.rotation;

    var x = Input.GetAxis("Horizontal") * Time.deltaTime * 3.0f;
    var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
    transform.Translate(x, 0, z);

    
    if (axes == RotationAxes.MouseXAndY) {
      float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
      
      rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
      rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
      
      transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
    } else if (axes == RotationAxes.MouseX) {
      transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
    } else {
      rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
      rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
      
      transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
    }

    //set circle boundary
    Vector3 newLocation = transform.localPosition;
    float distance = Vector3.Distance(newLocation, centerPosition);
    
    if(distance > radius){
      Vector3 fromOriginToObject = newLocation - centerPosition;
      fromOriginToObject *= radius / distance;
      newLocation = centerPosition + fromOriginToObject;
      transform.position = newLocation;
    }

    bulletSpawn.transform.forward = transform.forward;

    if (Input.GetKeyDown(KeyCode.Mouse0)) {
      CmdFire();
    }

    // tmp += Time.deltaTime;
    // Debug.Log(tmp);
    // if (tmp >= 2) {
    //   CmdFire();
    //   tmp = 0;
    // }
  }

	void OnDestroy (){
		GameObject.Find("Main Camera").gameObject.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
	}

  public override void OnStartLocalPlayer() {
    // Debug.Log(type);
  }

  public void SetColorByType () {
    switch (type) {
        case 1:
          this.GetComponent<MeshRenderer>().material.color = Color.green;
          break;
        case 2:
          this.GetComponent<MeshRenderer>().material.color = Color.red;
          break;
        case 3:
          this.GetComponent<MeshRenderer>().material.color = Color.blue;
          break;
        default:
          this.GetComponent<MeshRenderer>().material.color = Color.red;
          break;
    }
  }

  public void SetTextByType () {
    switch (type) {
        case 1:
          typeText.color = Color.green;
          typeText.text = "Doctor";
          break;
        case 2:
          typeText.color = Color.red;
          typeText.text = "Attack";
          break;
        case 3:
          typeText.color = Color.blue;
          typeText.text = "Defense";
          break;
        default:
          typeText.color = Color.red;
          typeText.text = "Attack";
          break;
    }
  }
}
