﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

  // Use this for initialization
  void Start() {

  }

  // Update is called once per frame
  void Update() {
		
  }

  void OnCollisionEnter(Collision collision) {
    Destroy(gameObject);
    // if (collision.gameObject.name == "Player(Clone)") return;
    var hit = collision.gameObject;
    var health = hit.GetComponent<Health>();
    if (health != null) {
      health.TakeDamage(50);
    }
  }
}
