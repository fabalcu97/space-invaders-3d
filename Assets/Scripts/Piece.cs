﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

	// 1 Doctor
	// 2 Attack
	// 3 Defense
	public int type;

	// Use this for initialization
	void Start () {

	}

	public void SetType (float rnd) {
		// Debug.Log(rnd);
		if (rnd < 0.33f) { // Doctor
			type = 1;
			gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
		} else if (rnd >= 0.33f && rnd < 0.66f) { // Atack
			type = 2;
			gameObject.GetComponent<MeshRenderer>().material.color = Color.red;
		} else if (rnd >= 0.66f) { // Defense
			type = 3;
			gameObject.GetComponent<MeshRenderer>().material.color = Color.blue;
		}

		Destroy(gameObject, 10.0f);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void DestroyPiece () {
		Destroy(this);
	}
}
