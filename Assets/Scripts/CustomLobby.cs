﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomLobby : NetworkLobbyManager {

	private List<int> types = new List<int>();
	// Use this for initialization
	void Start () {
		types.Add(1);
		types.Add(2);
		types.Add(3);

	}

	void Update () {

	}

	public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer) {
		var value = types[Random.Range(0, types.Count-1)];
		types.Remove(value);
		// Debug.Log("Server type =>" + value);
		gamePlayer.GetComponent<PlayerController>().SetType(value);
		// gamePlayer.GetComponent<PlayerController>().SetColorByType();
		lobbyPlayer.SetActive(false);
		// Debug.Log(lobbyPlayer.active);
		return true;
	}
}
