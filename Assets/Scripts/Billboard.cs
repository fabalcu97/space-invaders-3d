﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

	GameObject myCam;
	// Use this for initialization
	void Start () {
		myCam = GameObject.Find("Main Camera").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(myCam.transform);
	}
}
