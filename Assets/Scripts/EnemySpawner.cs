﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;

public class EnemySpawner : NetworkBehaviour
{

  public GameObject enemyPrefab;
  public int numberOfEnemies;
  public int degree;
  public int radius;
  private int increase;
  public double DEGTORAD(int deg){
    return Mathf.PI * deg / 180;
  }

  public override void OnStartServer() {
    radius = 30;
    increase = 360/numberOfEnemies;
    for (int i = 0; i < 360; i=i+increase){
      float x = (float)+Math.Cos(DEGTORAD(i)) * radius;
      float z = (float)+Math.Sin(DEGTORAD(i)) * radius;

      var spawnPosition = new Vector3(x, 0.0f, z);
      var spawnRotation = Quaternion.Euler(0.0f, 20.0f, 0.0f);
      var enemy = Instantiate(enemyPrefab, spawnPosition, spawnRotation);
      enemy.GetComponent<Enemy>().SetMovement((int)UnityEngine.Random.Range(-1, 1));
      NetworkServer.Spawn(enemy);
    }
  }

  public void Update() {

  }
}
