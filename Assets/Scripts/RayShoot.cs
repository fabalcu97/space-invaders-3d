using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class RayShoot : MonoBehaviour
{
  public int range = 1000;
  private LineRenderer line;
  public Transform bulletSpawn;
  RaycastHit hit;
  public bool playerOnly = true;

  PlayerController player;

  void Start()
  {
    line = GetComponent<LineRenderer>();
    player = gameObject.GetComponent<PlayerController>();
    line.positionCount = 2;
    line.startWidth = 0.1f;
    line.endWidth = 1.0f;
  }

  void Update() // consider void FixedUpdate()
  {
    if (!player.isLocalPlayer) {
      return;
    }
    SetLineColor();
    RaycastHit hit;
    Ray touchingRay = new Ray(bulletSpawn.transform.position, bulletSpawn.transform.forward);
    if (Input.GetMouseButton(1)) {
      line.SetPosition(0, bulletSpawn.transform.position);
      line.SetPosition(1, transform.forward * range);
      if (Physics.Raycast(touchingRay, out hit, range)) {
        Collider collider = hit.collider;
        if (collider) {
          handleCollision(collider);
        }
      }
    } else {
      line.SetPosition(0, Vector3.zero);
      line.SetPosition(1, Vector3.zero);
    }
  }

  void handleCollision(Collider collider) {
    var playerType = player.type;
    var collisionPiece = collider.GetComponent<Piece>();
    var collisionPlayer = collider.GetComponent<PlayerController>();
    if (collisionPiece != null) {
      if (playerType == 2 & collisionPiece.type == 2) {
        player.amo += 10;
        collisionPiece.DestroyPiece();
      } else if (playerType == 1 & collisionPiece.type == 1) {
        player.medicine += 25;
        collisionPiece.DestroyPiece();
      } else if (playerType == 3 & collisionPiece.type == 3) {
        player.defense += 15;
        collisionPiece.DestroyPiece();
      }
    } else if (collisionPlayer != null) {
      if (playerType == 2) {
        collisionPlayer.amo += player.amo;
      } else if (playerType == 1) {
        collisionPlayer.medicine += player.medicine;
      } else if (playerType == 3) {
        collisionPlayer.defense += player.defense;
      }
    }
  }

  void SetLineColor () {
    switch (player.type) {
        case 1:
          line.GetComponent<MeshRenderer>().material.color = Color.green;
          break;
        case 2:
          line.GetComponent<MeshRenderer>().material.color = Color.red;
          break;
        case 3:
          line.GetComponent<MeshRenderer>().material.color = Color.blue;
          break;
        default:
          line.GetComponent<MeshRenderer>().material.color = Color.red;
          break;
    }
  }
}